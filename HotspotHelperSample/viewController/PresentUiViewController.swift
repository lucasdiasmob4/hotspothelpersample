//
//  PresentUiViewController.swift
//  HotspotHelperSample
//
//  Created by Carlos Lucas da Silva Dias on 24/04/2019.
//  Copyright © 2019 Carlos Lucas da Silva Dias. All rights reserved.
//

import UIKit
import NetworkExtension

class PresentUiViewController: UIViewController {

    var presentUiCmd: NEHotspotHelperCommand!
    var hotspotHelperViewController: ViewController!

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func successButon(_ sender: Any) {
        NSLog("successButon")
        performSegue(withIdentifier: "success", sender: self)
    }
    
    @IBAction func temporatyFailButton(_ sender: Any) {
        NSLog("temporatyFailButton")
        performSegue(withIdentifier: "temporaryFail", sender: self)
    }
    
    @IBAction func failButton(_ sender: Any) {
        NSLog("failButton")
        performSegue(withIdentifier: "fail", sender: self)
    }
    // MARK: - Navigation

     //In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var viewControllers = navigationController?.viewControllers
        viewControllers?.removeLast(1)
        if(segue.identifier == "success"){
            NSLog("SEGUE - success")
            hotspotHelperViewController = segue.destination as? ViewController
            hotspotHelperViewController.presentUiResult = NEHotspotHelperResult.success
            hotspotHelperViewController.isForNetworksRegistered = true
            hotspotHelperViewController.presentUiCmd = presentUiCmd
            hotspotHelperViewController.presentUiResponse()
        }
        else if(segue.identifier == "temporaryFail"){
            NSLog("SEGUE - temporaryFail")
            hotspotHelperViewController = segue.destination as? ViewController
            hotspotHelperViewController.presentUiResult = NEHotspotHelperResult.temporaryFailure
            hotspotHelperViewController.isForNetworksRegistered = true
            hotspotHelperViewController.presentUiCmd = presentUiCmd
            hotspotHelperViewController.presentUiResponse()
        }
        else{
            NSLog("SEGUE - fail")
            hotspotHelperViewController = segue.destination as? ViewController
            hotspotHelperViewController.presentUiResult = NEHotspotHelperResult.failure
            hotspotHelperViewController.isForNetworksRegistered = true
            hotspotHelperViewController.presentUiCmd = presentUiCmd
            hotspotHelperViewController.presentUiResponse()
        }

    }
 

}
