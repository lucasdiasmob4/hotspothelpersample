//
//  ViewController.swift
//  HotspotHelperSample
//
//  Created by Carlos Lucas da Silva Dias on 15/04/2019.
//  Copyright © 2019 Carlos Lucas da Silva Dias. All rights reserved.
//

import UIKit
import NetworkExtension
import UserNotifications

class ViewController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate, UITableViewDelegate, UITableViewDataSource, UNUserNotificationCenterDelegate {

    enum Scenario {
        case authenticationLogoff, authenticationFail, authenticationTemporaryFail, authenticationSuccess, presentUi, maintainLogoff, maintainFail, maintainTemporaryFail, maintainSuccess
    }
    
    @IBOutlet weak var stateTableView: UITableView!
    var hotspotRegister = false
    var presentUiCmd: NEHotspotHelperCommand!
    var lastCmd: NEHotspotHelperCommand!
    var lastNetwork: NEHotspotNetwork?
    var scenarioList = [Scenario.authenticationLogoff, Scenario.authenticationFail, Scenario.authenticationTemporaryFail, Scenario.authenticationSuccess, Scenario.presentUi,
                        Scenario.maintainLogoff, Scenario.maintainFail, Scenario.maintainTemporaryFail, Scenario.maintainSuccess]
    @IBOutlet weak var stateMachineLabel: UILabel!
    @IBOutlet weak var chooseScenario: UIPickerView!
    var stateCount = 1
    var authenticationResult = NEHotspotHelperResult.failure
    var presentUiResult = NEHotspotHelperResult.failure
    var maintainResult = NEHotspotHelperResult.failure
    var isForNetworksRegistered = false
    var historicList = [String]()
    var maintainLogoff = false
    var authenticationLogoff = false
    
    private static var sharedInstance: ViewController = {
        let viewController = ViewController()
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //if(isForNetworksRegistered == false){
        //}
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.registerForNetworks()
        configureNotification()
        configurePickerView()
    }
    
    class func sharedIntance() -> ViewController {
        return sharedInstance
    }
    
    func configureNotification(){
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (didAllow, error) in }
            UNUserNotificationCenter.current().delegate = self
        } else {
            NSLog("ViewDidLoad - Versão menor que a 10")
        }
    }
    func configurePickerView(){
        self.chooseScenario.delegate = self as? UIPickerViewDelegate
    }

    func registerForNetworks () {
        //TO DO: Verificar se a String abaixo é o nome da rede, debugando no app da NET
        let options = [kNEHotspotHelperOptionDisplayName: "#NET-CLARO-WIFI" as NSString]
        self.hotspotRegister = NEHotspotHelper.register(options: options,
                                                        queue: DispatchQueue.main,
                                                        handler: self.handlerHotspots)

        NSLog("registerWithOptions result: \(self.hotspotRegister)")
        
        if ViewController.currentNetwork() != nil {
            self.lastNetwork = ViewController.currentNetwork()
        }
        
    }
    
    
    func handlerHotspots(cmd: NEHotspotHelperCommand) {
        self.lastCmd = cmd
        
        if cmd.network != nil {
            self.lastNetwork = cmd.network
            NSLog("\n\nNETWORK \(self.lastNetwork?.ssid ?? "")\nsignalStrength \(self.lastNetwork?.signalStrength ?? 0.0)\n\n")
        }
        
        switch (self.lastCmd.commandType) {
            
        case NEHotspotHelperCommandType.filterScanList:
            historicList.insert("\(stateCount) - FILTER_SCAN_LIST", at: historicList.count)
            stateTableView.reloadData()
            self.stateMachineLabel.text = ("\(stateCount) - FILTER_SCAN_LIST - \(historicList.count)")
            var list = [NEHotspotNetwork]()
            NSLog("HotspotHelper: FILTER_SCAN_LIST")
            stateCount += 1
            if(self.lastCmd.networkList != nil){
                for network in self.lastCmd.networkList! {
                    if network.ssid.contains("NET"){
                        //NSLog("FILTER_SCAN_LIST - \(network.ssid) - HIGH")
                        list.append(network)
                        network.setConfidence(.high)
                    }
                    else{
                        network.setConfidence(.none)
                    }
                }
            }
            let response = self.lastCmd.createResponse(NEHotspotHelperResult.success)
            response.setNetworkList(list)
            response.deliver()
            break
            
            
        case NEHotspotHelperCommandType.evaluate:
            historicList.insert("\(stateCount) - EVALUATE", at: historicList.count)
            stateTableView.reloadData()
            self.stateMachineLabel.text = ("\(stateCount) - EVALUATE - \(historicList.count)")
            NSLog("HotspotHelper: EVALUATE")
            //sleep(10)
            stateCount += 1
            let network = self.lastCmd.network!
            if network.ssid.contains("NET"){
                NSLog("Evaluate network \(network.ssid)")
                network.setConfidence(.high)
                let response = cmd.createResponse(NEHotspotHelperResult.success)
                response.setNetwork(network)
                response.deliver()
            }
            break
            
        case NEHotspotHelperCommandType.authenticate:
            NSLog("authenticationLogoff: \(authenticationLogoff)")
            historicList.insert("\(stateCount) - AUTHENTICATE", at: historicList.count)
            if (authenticationLogoff == true){
                if lastCmd.network != nil{
                    NEHotspotHelper.logoff(lastCmd.network!)
                }
            }
            else{
                stateTableView.reloadData()
                self.stateMachineLabel.text = ("\(stateCount) - AUTHENTICATE - \(historicList.count)")
                NSLog("HotspotHelper: AUTHENTICATE")
                NSLog("HotspotHelper: AUTHENTICATE with state \(stateString(state: authenticationResult.rawValue))")
                let response = cmd.createResponse(authenticationResult)
                response.setNetwork(cmd.network!)
                response.deliver()
            }
            stateCount += 1
            break
            
        case NEHotspotHelperCommandType.presentUI:
            self.presentUiCmd = cmd
            historicList.insert("\(stateCount) - PRESENT_UI", at: historicList.count)
            stateTableView.reloadData()
            self.stateMachineLabel.text = ("\(stateCount) - PRESENT_UI - \(historicList.count)")
            self.sendNotification(message: "PRESENT_UI")
            NSLog("HotspotHelper: PRESENT_UI")
            stateCount += 1
//            if lastCmd.network != nil{
//                NSLog("maintainLogoff: \(lastCmd.network!)")
//                NEHotspotHelper.logoff(lastCmd.network!)
//            }
            break
            
            
        case NEHotspotHelperCommandType.maintain:
            NSLog("maintainLogoff: \(maintainLogoff)")
            historicList.insert("\(stateCount) - MAINTAIN", at: historicList.count)
            self.stateMachineLabel.text = ("\(stateCount) - MAINTAIN - \(historicList.count)")
            if (maintainLogoff == true){
                if lastCmd.network != nil{
                    NSLog("maintainLogoff: \(lastCmd.network!)")
                    NEHotspotHelper.logoff(lastCmd.network!)
                }
            }
            else{
                NSLog("maintainResponse: \(maintainResult)")
                let response = cmd.createResponse(maintainResult)
                response.setNetwork(cmd.network!)
                response.deliver()
            }
            stateTableView.reloadData()
            NSLog("HotspotHelper: MAINTAIN")
            stateCount += 1
            break
            
        case NEHotspotHelperCommandType.logoff:
            historicList.insert("\(stateCount) - LOGOFF", at: historicList.count)
            stateTableView.reloadData()
            self.stateMachineLabel.text = ("\(stateCount) - LOGOFF - \(historicList.count)")
            NSLog("HotspotHelper: LOGOFF")
            stateCount += 1
            break
            
        case NEHotspotHelperCommandType.none:
            historicList.insert("\(stateCount) - NONE", at: historicList.count)
            stateTableView.reloadData()
            self.stateMachineLabel.text = ("\(stateCount) - NONE - \(historicList.count)")
            NSLog("HotspotHelper: NONE")
            stateCount += 1
            break
        }
    }
    
    func stateString (state: Int) -> String{
        switch state {
        case 0:
            return "SUCCESS"
        case 1:
            return "FAILURE"
        case 2:
            return "UI_REQUIRED"
        case 3:
            return "COMMAND_NOT_RECOGNIZED"
        case 4:
            return "AUTHENTICATION_REQUIRED"
        case 5:
            return "UNSUPORTED_NETWORK"
        case 6:
            return "TEMPORARY_FAILURE"
        default:
            return ""
        }
    }

    func commandeTypeString (state: Int) -> String{
        switch state {
        case 0:
            return "none"
        case 1:
            return "filterScanList"
        case 2:
            return "evaluate"
        case 3:
            return "authenticate"
        case 4:
            return "presentUI"
        case 5:
            return "maintain"
        case 6:
            return "logoff"
        default:
            return ""
        }
    }
    
    func scenarioString(scenario: Scenario)-> String{
        
        switch scenario {
        case Scenario.authenticationLogoff:
            return "authenticationLogoff"
            
        case Scenario.authenticationFail:
            return "authenticationFail"
            
        case Scenario.authenticationTemporaryFail:
            return "authenticationTemporaryFail"
            
        case Scenario.authenticationSuccess:
            return "authenticationSuccess"
            
        case Scenario.presentUi:
            return "presentUi"
            
        case Scenario.maintainLogoff:
            return "maintainLogoff"
            
        case Scenario.maintainFail:
            return "maintainFail"
            
        case Scenario.maintainTemporaryFail:
            return "maintainTemporaryFail"
            
        case Scenario.maintainSuccess:
            return "maintainSuccess"
        }
    }
    
    static func currentNetwork() -> NEHotspotNetwork? {
        if let interfaces = ViewController.networkInterfaces() {
            for network in interfaces {
                NSLog("currentNetwork \(network), interfaces (interfaces)")
                if (network.ssid != nil && !network.ssid.isEmpty) {
                    return network as? NEHotspotNetwork
                }
            }
        }
        return nil
    }
    
    static func networkInterfaces() -> [AnyObject]? {
        if let networks = NEHotspotHelper.supportedNetworkInterfaces() as? [NEHotspotNetwork] {
            if !networks.isEmpty {
                return networks
            }
        }
        return nil
    }
    
    func sendNotification(message: String!, param: String? = nil) {
        NSLog("NOTIFICOU!!!")
        
        let localNotification = UILocalNotification()
        localNotification.alertTitle = "PRESENT_UI"
//        localNotification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }
    
    func checkAuthenticationForCommand() {
        NSLog("checkAuthenticationForCommand")
        NSLog("NOTIFICOU!!! checkAuthenticationForCommand")
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return scenarioList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.view.endEditing(true)
        return scenarioString(scenario: scenarioList[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        NSLog("SCENARIO SELECTED: \(self.scenarioList[row])")
        self.authenticationLogoff = false
        self.maintainLogoff = false
        
        switch scenarioList[row] {
        case Scenario.authenticationFail:
            self.authenticationResult = NEHotspotHelperResult.failure
            break
            
        case Scenario.authenticationLogoff:
            self.authenticationLogoff = true
            break
            
        case Scenario.authenticationTemporaryFail:
            self.authenticationResult = NEHotspotHelperResult.temporaryFailure
            break
            
        case Scenario.authenticationSuccess:
            self.authenticationResult = NEHotspotHelperResult.success
            break
            
        case Scenario.presentUi:
            self.authenticationResult = NEHotspotHelperResult.uiRequired
            break
            
        case Scenario.maintainLogoff:
            self.authenticationResult = NEHotspotHelperResult.success
            self.maintainLogoff = true
            
        case Scenario.maintainFail:
            self.authenticationResult = NEHotspotHelperResult.success
            self.maintainResult = NEHotspotHelperResult.failure
            
        case Scenario.maintainTemporaryFail:
            self.authenticationResult = NEHotspotHelperResult.success
            self.maintainResult = NEHotspotHelperResult.temporaryFailure
            
        case Scenario.maintainSuccess:
            self.authenticationResult = NEHotspotHelperResult.success
            self.maintainResult = NEHotspotHelperResult.success
        }
        
        
    }
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return historicList.count
    }

    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = historicList[indexPath.row]
        return cell
    }

    func presentUiResponse(){
        let response = self.presentUiCmd.createResponse(presentUiResult)
        response.setNetwork(self.presentUiCmd.network!)
        NSLog("NEHotspotHelperResult \(response)")
        
        response.deliver()
    }
    
}

